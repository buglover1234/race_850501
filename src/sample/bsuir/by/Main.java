package sample.bsuir.by;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.net.URL;


public class Main extends Application {

    public static MediaPlayer mediaPlayer;

    @Override
    public void start(Stage primaryStage) throws Exception{

        URL location = getClass().getResource("ui/MainPage.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root = (Parent) fxmlLoader.load(location.openStream());
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        AppController app = AppController.getInstance();
        app.setAppStage(primaryStage);
        Database.connect();
        app.showStage();
        addMusic();
    }

    public void addMusic(){
        String path = "src/sample/bsuir/by/Resources/Music.wav";
        Media media = new Media(new File(path).toURI().toString());
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setAutoPlay(true);
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        mediaPlayer.setStartTime(Duration.seconds(0));
        mediaPlayer.play();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
