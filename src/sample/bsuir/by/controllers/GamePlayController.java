package sample.bsuir.by.controllers;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import sample.bsuir.by.AppController;
import sample.bsuir.by.Logic;

import java.io.IOException;
import java.util.ArrayList;


public class GamePlayController{

    private AnimationTimer animation;
    private AppController app;

    private FXMLLoader endGameLoader;
    private FXMLLoader menuLoader;
    @FXML
    private AnchorPane road;

    @FXML
    private Label scoreTable;

    public void scoreRender(int score){
        scoreTable.setText("Score: " + getScoreString(score));
    }

    public String getScoreString(int score){
        String scoreText = "" + score;
        if(scoreText.length() == 1)
            scoreText = "000" + scoreText;
        else
        if(scoreText.length() == 2)
            scoreText = "00" + scoreText;
        else
        if(scoreText.length() == 3)
            scoreText = "0" + scoreText;

        return scoreText;
    }

    public void endGame(Logic game) throws IOException {
        animation.stop();
        game.stopScore();
        road.setTranslateY(0);
        road.getChildren().removeAll();
    }

    void showGameOver(int score) throws IOException {
        app.setCurrentScene(endGameLoader.load());
        GameOverController controller = endGameLoader.getController();
        controller.setScore(score);
        app.showStage();
    }

    public void initLoaders(){
        endGameLoader = new FXMLLoader(getClass().getResource("../ui/GameOverMenu.fxml"));
        menuLoader = new FXMLLoader(getClass().getResource("../ui/GameMenu.fxml"));
    }

    public void start() throws Exception{
        this.app = AppController.getInstance();
        Logic game = new Logic();
        app.getAppStage().setOnCloseRequest(e -> {
            try {
                endGame(game);
                System.exit(0);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        ArrayList<String> input = new ArrayList<>();
        game.createContent(road);
        initLoaders();
        Scene thisScene = app.getCurrentScene();
        thisScene.setOnKeyPressed(e -> {
        String code = e.getCode().toString();

        if ( !input.contains(code) )
            input.add( code );
        });

        thisScene.setOnKeyReleased(e -> {
            String code = e.getCode().toString();
            input.remove( code );
        });

        game.runScoreTimer();
        game.checkCollision();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                game.update(road);
                game.handleKeys(input);

                if(game.isCrashed()){
                    try {
                        endGame(game);
                        showGameOver(game.getScore());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                //userCar.setDefaultPosition();
                scoreRender(game.getScore());
            }
        };
        animation = timer;
        timer.start();
        app.showStage();
    }
}
