package sample.bsuir.by.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import sample.bsuir.by.AppController;
import sample.bsuir.by.Database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Controller {
    AppController app;
    private FXMLLoader startGameLoader;

    public void initLoader(){
        startGameLoader = new FXMLLoader(getClass().getResource("../ui/GamePlay.fxml"));
    }

    @FXML
    private Button startGame;

    @FXML
    private AnchorPane mainRoot;

    @FXML
    void startGame(MouseEvent event) throws Exception{
        app = AppController.getInstance();
        initLoader();

        app.setCurrentScene(this.startGameLoader.load());
        GamePlayController controller = startGameLoader.getController();
        controller.start();
    }

    @FXML
    void endGame(MouseEvent event){
        app = AppController.getInstance();
        if(app.getAppStage() == null)
            System.out.println("null");
        app.getAppStage().close();
        System.exit(0);
    }

    @FXML
    private VBox liderBoard;

    @FXML
    void showLider() throws SQLException {
        ArrayList<String> arrayList = Database.getLiders();
        for(int i = 0; i < 5; i++) {
            Label label = new Label(arrayList.get(i));
            label.setFont(new Font("Segoe Script", 12));
            label.setTextFill(Paint.valueOf("#b91818"));
            liderBoard.getChildren().add(label);
        }
        liderBoard.setOpacity(1);
    }

}

