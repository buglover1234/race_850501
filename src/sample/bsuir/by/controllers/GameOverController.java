package sample.bsuir.by.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import sample.bsuir.by.AppController;
import sample.bsuir.by.Database;

import java.io.IOException;
import java.sql.SQLException;

public class GameOverController {
    @FXML
    private Label scoreResult;
    private int score;
    private AppController app;
    private FXMLLoader gameLoader;
    private FXMLLoader mainMenuLoader;

    public void setScore(int score){
        this.score = score;
        scoreResult.setText("" + score);
    }
    public void initLoaders(){
        if(gameLoader == null && mainMenuLoader == null) {
            gameLoader = new FXMLLoader(getClass().getResource("../ui/GamePlay.fxml"));
            mainMenuLoader = new FXMLLoader(getClass().getResource("../ui/MainPage.fxml"));
        }
    }

    @FXML
    void tryAgain(MouseEvent event) throws Exception {
        initLoaders();
        app = AppController.getInstance();
        app.setCurrentScene(gameLoader.load());
        GamePlayController controller = gameLoader.getController();
        controller.start();
    }

    @FXML
    void toMainMenu(MouseEvent event) throws IOException {
        initLoaders();
        app = AppController.getInstance();
        app.setCurrentScene(mainMenuLoader.load());
        app.showStage();
    }

    @FXML
    private TextField input;

    @FXML
    void addToDatabase() throws SQLException {
        if(!input.getText().equals(""))
            Database.addLider(input.getText(), score);
    }
}
