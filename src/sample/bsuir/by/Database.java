package sample.bsuir.by;
import java.sql.*;
import java.util.ArrayList;

public class Database {

    private static Connection connection;

    public static void connect() {
        try{
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:D:\\JOB\\labs\\Race\\src\\sample\\bsuir\\by\\users.db");
            System.out.println("connected");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void addUser(String name) throws SQLException {
        String query =
                "INSERT INTO users SET name " +
                "= " + name + ";";
        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    public static void addLider(String name, int score) throws SQLException {
        String query =
                "INSERT INTO users (name, score) " +
                "VALUES ('" + name + "','" + score + "');";
        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    public static ArrayList<String> getLiders() throws SQLException {
        ArrayList<String> liderList = new ArrayList<>();
        String query =
                "SELECT * FROM users ORDER BY score DESC;";
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(query);
        while(rs.next()){
            String lider = rs.getString("name")+ "    " + rs.getInt("score");
            liderList.add(lider);
        }
        return liderList;
    }

    public static void close() throws SQLException {
        connection.close();
    }

}
