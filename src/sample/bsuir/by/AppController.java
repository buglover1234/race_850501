package sample.bsuir.by;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class AppController {
    private static AppController instance;
    private AppController() {}
    public static AppController getInstance(){
        if(instance == null){
            instance = new AppController();
        }
        return instance;
    }

    private Stage appStage;
    private Scene currentScene;

    public void setAppStage(Stage stage){
        this.appStage = stage;
    }

    public Stage getAppStage(){
        return  this.appStage;
    }

    public void setCurrentScene(Parent root) throws IOException {
        this.appStage.hide();

        if(currentScene == null)
            currentScene = new Scene(root);
        else {
            currentScene.setRoot(root);
        }

        this.appStage.setScene(this.currentScene);
        this.appStage.sizeToScene();
        this.appStage.centerOnScreen();
        //this.appStage.show();
    }

    public Scene getCurrentScene(){
        return this.currentScene;
    }

    public void showStage(){
        this.appStage.show();
    }
}
