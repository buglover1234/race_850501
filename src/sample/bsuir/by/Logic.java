package sample.bsuir.by;

import javafx.scene.layout.AnchorPane;
import sample.bsuir.by.models.Car;
import sample.bsuir.by.models.UserCar;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Logic {

    private static final int LINE1 = 77;
    private static final int LINE2 = 177;
    private static final int LINE3 = 277;
    private static final String GREEN = "sample/bsuir/by/Resources/PNG/Cars/car_green_4.png";
    private static final String BLUE = "sample/bsuir/by/Resources/PNG/Cars/car_blue_3.png";
    private static final String RED = "sample/bsuir/by/Resources/PNG/Cars/car_red_small_2.png";
    private UserCar userCar;
    private int speed;
    private boolean crashed;

    private Timer scoreTimer;

    private int score;
    public ArrayList<Car> carList = new ArrayList<>();

    public void createContent(AnchorPane road){
        Integer line = 0;
        int buf = 0;
        for(int i = 0; i < 1000; i++){
            int carCount = (int)(Math.random() * 10) % 2 + 1;
            for(int j = 0; j < carCount; j++) {
                if(j == 1)
                {
                    if((buf = this.generateLine()) != line)
                        line = buf;
                    else{
                        while((buf = generateLine()) == line);
                        line = buf;
                    }
                    Car car = new Car(line, (i * (-220) - 70), generateCar(), 49, 69);
                    carList.add(car);
                    road.getChildren().addAll(car);
                }
                else{
                    line = generateLine();
                    Car car = new Car(line, (i * (-220) - 70), generateCar(), 49, 69);
                    carList.add(car);
                    road.getChildren().addAll(car);
                }

            }
            line = 0;
        }
        this.speed = 3;
        userCar = new UserCar(speed);
        crashed = false;
        road.getChildren().addAll(userCar);

    }

    public Integer generateLine(){
        int r = (int)(Math.random()*10 % 3);
        switch (r){
            case 0:{
                return LINE1;
            }
            case 1:{
                return LINE2;
            }
            case 2: {
                return LINE3;
            }
        }
        return null;
    }


    public String generateCar(){
        int r = (int)(Math.random()*10 % 3);
        switch (r){
            case 0: {
                return GREEN;
            }
            case 1:{
                return BLUE;
            }
            case 2:{
                return RED;
            }
        }
        return null;
    }

    public void handleKeys(ArrayList<String> input){

        if(input.contains("LEFT"))
            userCar.moveLeft();

        if(input.contains("RIGHT"))
            userCar.moveRight();

    }

    public void runScoreTimer(){
        scoreTimer = new Timer();
        TimerTask updateScore = new TimerTask() {
            @Override
            public void run() {
                if (score % 20 == 0)
                    increaseSpeed(1);
                score += 1;
                System.out.println(score);
            }
        };
        scoreTimer.schedule(updateScore, 300, 300);
    }

    public void increaseSpeed(int add){
        speed += add;
        userCar.setSpeed(speed);
    }

    public void stopScore(){
        scoreTimer.cancel();
    }

    public boolean isCrashed(){
        return crashed;
    }

    public int getScore(){
        return score;
    }

    public void checkCollision(){
        Runnable check = () -> {
            while (!crashed) {
                carList.forEach(car -> {
                    if (car.checkCollision(userCar)) {
                        crashed = true;
                    }
                });
            }
        };
        Thread thread = new Thread(check);
        thread.start();
    }

    public void update(AnchorPane road) {
        road.setTranslateY(road.getTranslateY() + speed);
        userCar.move();
    }


}
