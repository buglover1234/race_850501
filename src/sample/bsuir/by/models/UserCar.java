package sample.bsuir.by.models;

public class UserCar extends Car {
    private int speed;
    private boolean crashed;

    public UserCar(int speed){
        super(177, 500, "sample/bsuir/by/Resources/PNG/Cars/car_yellow_1.png", 59, 69);
        this.speed = speed;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public void setSpeed(int speed) { this.speed = speed; }

    public void moveLeft(){
        if(this.getX() - this.speed > 50)
            this.setX(this.getX() - this.speed * 2);
    }

    public void moveRight(){
        if(this.getX() - this.speed < 300)
            this.setX(this.getX() + this.speed * 2);
    }

    public void move(){
        this.setY(this.getY() - this.speed);
    }
}
