package sample.bsuir.by.models;

import javafx.scene.image.ImageView;

public class Car extends ImageView{

    private int speed;

    public Car(int x, int y, String path, int width, int height){
        super(path);
        this.setX(x);
        this.setY(y);
        this.setFitWidth(width);
        this.setFitHeight(height);
    }

    public boolean checkCollision(Car car){
        return car.getBoundsInLocal().intersects(this.getBoundsInLocal());
    }


}
